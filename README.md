#Energistics MDG

Model-Driven Generation tools for use in Sparx Enterprise Architect.

[Wiki](mdg-eml.git/wiki/Home)

This project contains the source EAP and the generated output for the Energistics MDG. MDG is short for Model Driven Generation, and when you use Sparx EA, you are using MDG technologies for the various types of models you create (ArchiMate, TOGAF, etc.). This MDG is a set of tools designed specifically to accelerate the development and generation process for schemas in the Energistics family of data transfer specifications.

##Installation

Use the Settings menu in EA manage MDG Technologies

![2014-05-02_1148.png](https://bitbucket.org/repo/K5qoMG/images/2395184032-2014-05-02_1148.png)

The following dialog will appear, click on the Advanced button, and then Add:

![2014-05-02_1201.png](https://bitbucket.org/repo/K5qoMG/images/2137871474-2014-05-02_1201.png)

1. If you have cloned this project from Git, add as a path and just browse to the root location of the git repository:
	
	c:\[location of repository]\ 

2. If you just want to get it from the internet, add as a url, and use the following:

	https://bitbucket.org/energistics/mdg-eml/raw/master/EnergisticsMDG.xml

If the Energistics MDG shows up, with the Energistcs Logo, and the box is checked, you are good to go.