<MDG.Selections model="C:\Projects\MDG\EnergisticsMDG.EAP">
	<Technology id="EML" name="Energistics" version="0.0.4" notes="Energistics' customization of Sparx Enterprise Architecture tool to support standards development." filename="C:\EnergisticsRoot\energyml\tools\EnergisticsMDG.xml" icon="C:\EnergisticsRoot\energyml\tools\src\logo-energistics-small.bmp" logo="C:\EnergisticsRoot\energyml\tools\src\logo-energistics-large.bmp"/>
	<Profiles directory="F:\Standards\Energistics\energyml\tools\mdg-eml\src" files=""/>
	<TagVals tags="AvroSchema,GenerationDirectory,Namespace,StabilityIndex,XsdNoGenerate"/>
	<CodeMods>
		<CodeMod product="Avro" codetypes="1" codetemplates="1" grammar="" codeoptions="C:\EnergisticsRoot\energyml\tools\src\AvroCodeOptions.xml"/>
	</CodeMods>
	<DiagramProfile directory="F:\Standards\Energistics\energyml\tools\mdg-eml\src" files=""/>
	<UIToolboxes directory="F:\Standards\Energistics\energyml\tools\mdg-eml\src" files=""/>
	<ModelSearches2/>
	<RTFTemplates>
		<Template name="Energistics Base"/>
		<Template name="Ralf Specification"/>
	</RTFTemplates>
	<Scripts>
		<Script id="{952B88A3-E63D-479b-AE51-6CB8BCEAC21F}" name="Resqml::CreateSchemas" type="normal" language="JScript"/>
		<Script id="{18BCDC26-1EC5-4de1-B55B-58AB72FC957B}" name="ETP::GenerateSchemas" type="normal" language="JScript"/>
		<Script id="{6A28CAD5-D1AD-40d5-9016-EAC6CAC410DD}" name="Common::XSDattribute usage" type="normal" language="JScript"/>
		<Script id="{3A40AA80-0562-46f0-8DED-E8A32607E9DD}" name="Resqml::CheckAttributeTypes" type="normal" language="JScript"/>
		<Script id="{7A7F0B3B-73BE-4595-AA10-3A753DE554ED}" name="ConversionUtil" type="normal" language="JScript"/>
		<Script id="{DD1E6F6D-37B6-4647-B360-1F23EFAFA486}" name="Common::FileUtil" type="normal" language="JScript"/>
		<Script id="{70769359-DA37-48b6-BECD-C6680330C52B}" name="Common::PackageUtil" type="normal" language="JavaScript"/>
		<Script id="{B12BEE6D-09B9-428a-BB31-C688B26E77DB}" name="Resqml::SetNamespace" type="normal" language="JScript"/>
		<Script id="{79B458F8-4A66-42f0-B72E-5DBFE76C1A5E}" name="Common::ReportingUtil" type="normal" language="JScript"/>
		<Script id="{358B80CD-2289-452e-A7E6-727ACDC96577}" name="ETP::CreateAvroProtocolFile" type="normal" language="JScript"/>
		<Script id="{DF556920-2CBC-4005-9FA0-46AF4EB4486D}" name="Common::Prototypes" type="normal" language="JScript"/>
		<Script id="{BE092E4D-08BA-4f17-ACD3-0563D7523083}" name="Common::SchemaUtil" type="normal" language="JScript"/>
		<Script id="{B7386C08-EFC0-4af5-B998-7A33D2F71A50}" name="Common::SetNamespace" type="normal" language="JScript"/>
		<Script id="{A5BBF3EA-64EA-4bef-8577-D16424B9D4AB}" name="Common::AttributeClassification" type="normal" language="JScript"/>
		<Script id="{0695145E-EDB2-460c-82C6-149EDA519A58}" name="Common::GenerateSchemas::v21" type="normal" language="JScript"/>
		<Script id="{4008B114-6693-4bfc-8D7D-536CBFB7039E}" name="ETP::SetNamespace" type="normal" language="JScript"/>
	</Scripts>
</MDG.Selections>
